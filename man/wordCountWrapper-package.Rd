\name{wordCountWrapper-package}
\alias{wordCountWrapper-package}
\alias{wordCountWrapper}
\docType{package}
\title{
Word Count Wrapper
}
\description{
This package wraps a few word counting methods for downstream analysis with other tools
}
\details{
\tabular{ll}{
Package: \tab wordCountWrapper\cr
Type: \tab Package\cr
Version: \tab 1.0\cr
Date: \tab 2014-03-06\cr
License: \tab GPL\cr
}
TODO: ADD brief usage
}
\author{
Mark Peterson

Maintainer: Mark Peterson <mark.phillip.peterson@gmail.com>
}
\references{
~~ Literature or other references for background information ~~
}
% ~~ Optionally other standard keywords, one per line, from file KEYWORDS in the R documentation ~~
% ~~ directory ~~
\keyword{ package }
\seealso{
%~~ Optional links to other man pages, e.g. ~~
%~~ \code{\link[<pkg>:<pkg>-package]{<pkg>}} ~~
}
\examples{
% ~~ simple examples of the most important functions ~~
## TODO: add examples
}
